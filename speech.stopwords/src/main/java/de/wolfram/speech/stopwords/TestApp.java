package de.wolfram.speech.stopwords;


public class TestApp
{

    public static void main(String[] args) throws Throwable
    {
        StopWords stopWords = new StopWords();
        stopWords.load();
        String s = "Peter öffne bitte youtube.com";
        String removedWords = stopWords.remove(s);
        System.out.println(removedWords);
    }
}
