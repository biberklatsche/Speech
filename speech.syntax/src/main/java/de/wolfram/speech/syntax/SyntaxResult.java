package de.wolfram.speech.syntax;

import java.util.EnumMap;

/**
 *
 * @author Lars
 */
public class SyntaxResult {
    private final String word;
    private EnumMap<SyntaxType, Long> probability;

    public SyntaxResult(String word){
        this.word = word;
    }
    
    public String getWord() {
        return word;
    }

    public EnumMap<SyntaxType, Long> getProbability() {
        return probability;
    }

    public void setProbability(EnumMap<SyntaxType, Long> probability) {
        this.probability = probability;
    }
    
}
