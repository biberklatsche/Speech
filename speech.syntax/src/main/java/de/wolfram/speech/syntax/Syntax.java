package de.wolfram.speech.syntax;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

/**
 *
 * @author Lars
 */
public class Syntax {

    private final Set<String> knownObjects = new TreeSet();
    private final Set<String> knownSubjects = new TreeSet();
    private final Set<String> knownPredicates = new TreeSet();
    
    public List<SyntaxResult> create(String s){
        return Collections.emptyList();
    }
}
