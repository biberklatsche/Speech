package de.wolfram.speech.phonetic;

import org.apache.commons.codec.language.ColognePhonetic;

public class TestApp
{

    public static void main(String[] args) throws Throwable
    {
        ColognePhonetic cp = new ColognePhonetic();
        String t1 = cp.colognePhonetic("Peter");
        String t2 = cp.colognePhonetic("öffne");
        String t3 = cp.colognePhonetic("bitte");
        String t4 = cp.colognePhonetic("youtube.com");
        String t5 = cp.colognePhonetic("youdub.com");
        
        System.out.println(t1 + " " + t2 + " " + t3 + " " + t4 + " " + t5);
    }
}
